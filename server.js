const express = require('express')
const app = express()
const path = require('path')
const browserify = require('browserify-middleware')
const cors = require('cors')
const morgan = require('morgan')
const opn = require('opn')

const PORT = 8118

app.use(cors())
app.use(morgan('combined'))

app.get('/', function (req, res, next) {
  res.sendFile(path.join(__dirname + '/index.html'))
})

app.get('/index.js', browserify(__dirname + '/app.js'));

app.get('/icon.png', function (req, res, next) {
  res.sendFile(path.join(__dirname + '/icon.png'))
})

app.get('/manifest.json', function (req, res, next) {
  res.sendFile(path.join(__dirname + '/manifest.json'))
})

app.get('/favicon.ico', function (req, res, next) {
  res.sendFile(path.join(__dirname + '/favicon.ico'))
})

app.listen(PORT, function () {
  console.log('Server started http://localhost:' + PORT)
  opn('http://localhost:' + PORT)
})
