# Teacup

## Installation

Have [Node](https://nodejs.org/) installed on your system then run:
```
npm install
```

## Run

In your terminal run:
```
node server.js
```

## Open

Browse to [http:localhost:8118](http:localhost:8118).

## Usage

Click on the "Sign In" button to authenticate with Blockstack.

## Files

- `.gitignore` A list of files and directories for the Git version control system to ignore.
- `app.js` The client side JS compiled by Browserify.
- `favicon.ico` Quickly generated browser tab icon to remove error from browser console.
- `index.html` The initial html.
- `package.json` The Node modules being used.
- `README.md` This file you are reading.
- `server.js` Contains an Express server with CORS for any domain enabled. It serves up the html at `/` and the Browserify transpiled JS at `/index.js`.

## Attribution

Icon made by modifying [the following image](https://commons.wikimedia.org/wiki/File:Tea_Cup_with_reflection.jpg).
