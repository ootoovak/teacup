var blockstack = require('blockstack')
var blockstackStorage = require('blockstack-storage')

console.log('Loading...')

var Auth = {

  signIn: function (event) {
    event.preventDefault()
    console.log('Signing in...')
    blockstack.redirectToSignIn()
  },

  signOut: function () {
    event.preventDefault()
    console.log('Signing out...')
    console.log('Sign out location:', window.location.origin)
    blockstack.signUserOut(window.location.origin)
  }

}

var View = {

  signInButton: null,
  signOutButton: null,
  signedIn: null,
  signedOut: null,
  name: null,
  avatar: null,

  findElements: function () {
    this.signInButton = document.querySelector('#sign-in')
    this.signOutButton = document.querySelector('#sign-out')
    this.signedIn = document.querySelector('#signed-in')
    this.signedOut = document.querySelector('#signed-out')
    this.name = document.querySelector('#name')
    this.avatar = document.querySelector('#avatar')
  },

  registerListeners: function () {
    this.signInButton.addEventListener('click', Auth.signIn)
    this.signOutButton.addEventListener('click', Auth.signOut)
  },

  showUser: function (user) {
    console.log('Showing user:', user)
    var profile = user.profile
    this.name.innerHTML = profile.givenName + ' ' + profile.familyName
    this.signedIn.style.display = 'block'
    this.signedOut.style.display = 'none'
  },

  redirect: function() {
    console.log('Assigning location:', window.location.origin)
    window.location = window.location.origin
  },

  hideUser: function () {
    console.log('Hiding user.')
    this.signedIn.style.display = 'none'
    this.signedOut.style.display = 'block'
  }

}

var App = {

  run: function () {
    View.findElements()
    View.registerListeners()

    if (blockstack.isUserSignedIn()) {
      console.log('User is signed in.')
      var user = blockstack.loadUserData()
      View.showUser(user)
    } else if (blockstack.isSignInPending()) {
      console.log('Sign in is pending.')
      blockstack.handlePendingSignIn(View.redirect)
    }
  }
}

document.addEventListener('DOMContentLoaded', App.run);

console.log('Done.')
